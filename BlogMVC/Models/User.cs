﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogMVC.Models
{
    public class User
    {
        public int Id{ get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Enter Only Word Please")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string UserEmail { get; set; }

        public virtual Collection<Post> Posts { get; set; }
        public virtual Collection<Comment> Comments { get; set; }
        public virtual Collection<Like> Likes { get; set; }
    }
}