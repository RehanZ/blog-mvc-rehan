﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogMVC.Models;

namespace BlogMVC.DAL.Interfaces
{
    public interface IUserRepo
    {
        void CreateUser(User user);
        void EditUser(User user);
        void DeleteUser(User user);
    }
}
