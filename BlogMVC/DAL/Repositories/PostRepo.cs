﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BlogMVC.DAL.Interfaces;
using BlogMVC.Models;
using BlogMVC.MyDatabase;

namespace BlogMVC.DAL.Repositories
{
    public class PostRepo: IPostRepo
    {
        MyContext _myContext=new MyContext();

        public void CreatePost(Post post)
        {
            _myContext.Posts.Add(post);
            _myContext.SaveChanges();
        }

        public void EditPost(Post post)
        {
            _myContext.Entry(post).State = EntityState.Modified;
            _myContext.SaveChanges();
        }

        public void DeletePost(Post post)
        {
            _myContext.Posts.Attach(post);
            _myContext.Posts.Remove(post);
            _myContext.SaveChanges();
        }
    }
}