﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using BlogMVC.DAL.Interfaces;
using BlogMVC.Models;
using BlogMVC.MyDatabase;

namespace BlogMVC.DAL.Repositories
{
    public class UserRepo : IUserRepo

    {
        private MyContext _myContext = new MyContext();
        public void CreateUser(User user)
        {
            _myContext.Users.Add(user);
            _myContext.SaveChanges();
        }

        public void EditUser(User user)
        {
            _myContext.Entry(user).State = EntityState.Modified;
            _myContext.SaveChanges();
        }

        public void DeleteUser(User user)
        {
            _myContext.Users.Attach(user);
            _myContext.Users.Remove(user);
            _myContext.SaveChanges();
        }
    }
}