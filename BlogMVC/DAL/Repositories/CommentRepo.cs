﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BlogMVC.DAL.Interfaces;
using BlogMVC.Models;
using BlogMVC.MyDatabase;

namespace BlogMVC.DAL.Repositories
{
    public class CommentRepo: ICommentRepo
    {
        private MyContext _myContext = new MyContext();
        public void CreateComment(Comment comment)
        {
            _myContext.Comments.Add(comment);
            _myContext.SaveChanges();
        }

    public void EditComment(Comment comment)
        {
            _myContext.Entry(comment).State = EntityState.Modified;
            _myContext.SaveChanges();
        }

        public void DeleteComment(Comment comment)
        {
            _myContext.Comments.Attach(comment);
            _myContext.Comments.Remove(comment);
            _myContext.SaveChanges();
        }
    }
}