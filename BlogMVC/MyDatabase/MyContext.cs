﻿using System.Data.Entity;
using BlogMVC.Models;

namespace BlogMVC.MyDatabase
{
    public class MyContext : DbContext
    {
        public MyContext() : base("DefaultConnection")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Like> Likes { get; set; }
    }
}