﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogMVC.ViewModel
{
    public class CommentsViewModel
    {
        public int Id { get; set; }
        public string CommentBody { get; set; }
        public int UserId { get; set; }
        public int PostId { get; set; }
    }
}