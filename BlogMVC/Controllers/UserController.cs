﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogMVC.DAL.Repositories;
using BlogMVC.Models;
using BlogMVC.MyDatabase;

namespace BlogMVC.Controllers
{
    public class UserController : Controller
    {
        MyContext _myContext = new MyContext();
        UserRepo _userRepo = new UserRepo();

        // GET: User
        public ActionResult Index()
        {
            return View(_myContext.Users.ToList());
        }

        public ActionResult Create()
        {          
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,UserName,UserEmail")] User user)
        {
            _userRepo.CreateUser(user);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int Id)
        {
            User user = _myContext.Users.Find(Id);

            return View(user);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,UserName,UserEmail")] User user)
        {
            _userRepo.EditUser(user);
            return RedirectToAction("Index");
        }


        public ActionResult Delete(int Id)
        {
            User user = _myContext.Users.Find(Id);
            return View(user);
        }

        [HttpPost]
        public ActionResult Delete([Bind(Include = "Id,UserName,UserEmail")] User user)
        {
            _userRepo.DeleteUser(user);
            return RedirectToAction("Index");
        }
    }
}