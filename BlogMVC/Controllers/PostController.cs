﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogMVC.DAL.Repositories;
using BlogMVC.Models;
using BlogMVC.MyDatabase;
using BlogMVC.ViewModel;

namespace BlogMVC.Controllers
{
    public class PostController : Controller
    {
        MyContext _myContext = new MyContext();
        PostRepo _postRepo=new PostRepo();

        // GET: Post
        public ActionResult Index()
        {
            return View(_myContext.Posts.ToList());
        }


        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(_myContext.Users, "Id", "UserName");
            return View();
        }


        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,PostTitle,PostBody,PostDate,UserId")] Post post)
        {
            _postRepo.CreatePost(post);
            return RedirectToAction("Index");
        }


        public ActionResult Edit(int Id)
        {
            Post post = _myContext.Posts.Find(Id);
            ViewBag.UserId = new SelectList(_myContext.Users, "Id", "UserName");
            return View(post);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,PostTitle,PostBody,PostDate,UserId")] Post post)
        {
            _postRepo.EditPost(post);
            return RedirectToAction("Index");
        }


        public ActionResult Delete(int Id)
        {
            Post post = _myContext.Posts.Find(Id);
            return View(post);
        }

        [HttpPost]
        public ActionResult Delete([Bind(Include = "Id,PostTitle,PostBody,PostDate,UserId")] Post post)
        {
            _postRepo.DeletePost(post);
            return RedirectToAction("Index");
        }


       
        public ActionResult Detail(int Id)
        {
            var post = _myContext.Posts.Find(Id);
            return View(post);
        }


        public PartialViewResult AllComments(int id)
        {
            List<CommentsViewModel> model = new List<CommentsViewModel> ();
            model = (from comments in _myContext.Comments where comments.PostId == id select new CommentsViewModel()
                {
                    Id = comments.Id,
                    CommentBody = comments.CommentBody,
                    UserId = comments.UserId,
                    PostId = comments.PostId
                }).ToList();

            return PartialView("~/Views/Comment/Index.cshtml", model);
        }


    }
}