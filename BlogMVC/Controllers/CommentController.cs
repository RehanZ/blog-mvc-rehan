﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogMVC.DAL.Repositories;
using BlogMVC.Models;
using BlogMVC.MyDatabase;

namespace BlogMVC.Controllers
{
    public class CommentController : Controller
    {
        MyContext _myContext = new MyContext();
        CommentRepo _commentRepo = new CommentRepo();
        

        // GET: Comment
        public ActionResult Index()
        {
            return View();
        }

       


        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(_myContext.Users, "Id", "UserName");
            ViewBag.PostId = new SelectList(_myContext.Posts, "Id", "PostTitle");
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,CommentBody,PostId,UserId")] Comment comment)
        {
            _commentRepo.CreateComment(comment);
            return RedirectToAction("Index");
        }
    }
}